package prv.app.login.entity;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

import static prv.app.login.entity.Authority.AUTHORITY;

@Entity
@Table(name = AUTHORITY)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class Authority implements GrantedAuthority {
    public static final String AUTHORITY = "prv_authority";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String authority;


}
