package prv.app.login.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;

import java.time.LocalDate;
import java.util.Date;

import static prv.app.login.entity.MioInterceptor.INTERCEPTOR;

@Entity
@Table(name = INTERCEPTOR)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class MioInterceptor {
    public static final String INTERCEPTOR = "prv_interceptor";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(columnDefinition="TIMESTAMP(6)")
    @Temporal(TemporalType.DATE)
    private Date callDate;
    private String srvName;
    private String ip;
}
