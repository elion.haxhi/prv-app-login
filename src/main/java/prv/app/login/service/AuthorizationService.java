package prv.app.login.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import prv.app.login.entity.User;
import prv.app.login.repository.UserRepository;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Slf4j
@Service
public class AuthorizationService {

    Map<String, Object> header = new HashMap<>();

    public static final String SECRET = "SECRET_KEY";

    @Inject
    UserRepository userRepository;

    public AuthorizationService(){

        header.put("alg","HS256");
        header.put("typ","JWT");

    }

    private String hmacSha256(String data, String secret) {
        try {

            byte[] hash = secret.getBytes(StandardCharsets.UTF_8);

            Mac sha256Hmac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKey = new SecretKeySpec(hash, "HmacSHA256");
            sha256Hmac.init(secretKey);

            byte[] signedBytes = sha256Hmac.doFinal(data.getBytes(StandardCharsets.UTF_8));
            return encode(signedBytes);
        } catch (NoSuchAlgorithmException | InvalidKeyException ex) {
            Logger.getLogger(AuthorizationService.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return null;
        }
    }

    private static String encode(byte[] bytes) {
        return Base64.getUrlEncoder().withoutPadding().encodeToString(bytes);
    }

    public Optional<User> findUserByUsername(String username) {

        User user = userRepository.readByUsername(username);

        return  Optional.ofNullable(user);
    }

    public String generateToken(String username, String password) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("username",username);
        payload.put("iat", LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));

        String credentials = username.concat(".").concat(password).concat(" ");

        String headerPlusPayload = encode(header) +"." + encode(credentials);

        String signature =  hmacSha256(headerPlusPayload, SECRET);

        return encode(header) + "." + encode(credentials) + "." + signature;

    }

    public String encode(Map<String, Object> map) {
        return encode(map.toString().getBytes(StandardCharsets.UTF_8));
    }

    public String encode(String plainText) {
        return encode(plainText.getBytes(StandardCharsets.UTF_8));
    }


    private static String decode(String encodedString) {

        return new String(Base64.getUrlDecoder().decode(encodedString));
    }

    public boolean verifyToken(String token) {
        String[] parts = token.split("\\.");
        String encodedHeader = "";

        if(parts.length != 3){
            return false;
        } else {

            String signature = parts[2];
            String payload = decode(parts[1]);

            if(parts[0].equals(encode(header)))
                encodedHeader = parts[0];
            if(signature.equals(hmacSha256(encodedHeader + "." + encode(payload.getBytes()),SECRET)))
                return true;
            else
                return false;

        }
    }
}
