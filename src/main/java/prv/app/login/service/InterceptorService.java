package prv.app.login.service;

import org.springframework.stereotype.Service;
import prv.app.login.entity.MioInterceptor;
import prv.app.login.repository.InterceptorRepository;
import prv.app.login.resource.MioTrack;
import prv.app.login.resource.ResponseModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InterceptorService {

    private InterceptorRepository interceptorRepository;

    InterceptorService(InterceptorRepository interceptorRepository){
        this.interceptorRepository = interceptorRepository;
    }

    public MioInterceptor invokeInterceptor(Date date, String srvName, String ip) {

        MioInterceptor mioInterceptor = new MioInterceptor();
        mioInterceptor.setCallDate(date);
        mioInterceptor.setSrvName(srvName);
        mioInterceptor.setIp(ip);

        return interceptorRepository.save(mioInterceptor);

    }

    public List<MioTrack> getAllMioInterceptorByCallDate(String filteredDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        List<MioInterceptor> interceptors = interceptorRepository.trovaPerCallDate(sdf.parse(filteredDate));
        return convertToMioTrack(interceptors);
    }

    private List<MioTrack> convertToMioTrack(List<MioInterceptor> interceptors) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        List<MioTrack> result =  interceptors.stream().map(m -> {
            return  MioTrack.builder()
                    .callDate(sdf.format(m.getCallDate()))
                    .ip(m.getIp())
                    .srvName(m.getSrvName())
                    .build();
        }).collect(Collectors.toList());
        return result;
    }

    public ResponseModel<List<MioInterceptor>> parseToDto(List<MioInterceptor> filteredMioInterceptor) {
        return ResponseModel.of(filteredMioInterceptor);
    }

    public List<MioTrack> getAll() {
        List<MioInterceptor> interceptors = interceptorRepository.findAll();
        return convertToMioTrack(interceptors);
    }
}
