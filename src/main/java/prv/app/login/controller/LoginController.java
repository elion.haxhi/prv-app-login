package prv.app.login.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.ui.Model;
import javax.servlet.http.HttpSession;

@Slf4j
@Controller
public class LoginController {

    @GetMapping(value = "/")
    public String index(HttpServletRequest request){



        return "index";
    }

    @PostMapping(value = "/login")
    public String login(HttpServletRequest request, Model model){

        HttpSession httpSession = request.getSession();
        if(httpSession != null && !(Boolean) httpSession.getAttribute("authenticated")){
            model.addAttribute("error","Username or password incorrect");
            return "index";
        }

        return "home";

    }

}
