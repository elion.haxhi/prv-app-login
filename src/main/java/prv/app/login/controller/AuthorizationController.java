package prv.app.login.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import prv.app.login.entity.User;
import prv.app.login.resource.RequestWorkOrderFixing;
import prv.app.login.resource.ResponseModel;
import prv.app.login.resource.ResponseWorkOrderFixing;
import prv.app.login.service.AuthorizationService;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/")
@Api(value = "token")
public class AuthorizationController {



    AuthorizationService authorizationService;

    public AuthorizationController(AuthorizationService authorizationService){
        this.authorizationService = authorizationService;
    }


    // we verify the validity of the token
    @GetMapping(path = "verifyToken")
    @ApiOperation(value = "retrieve user")
    public ResponseModel<Boolean> checkToken(@RequestHeader String jwtToken) {

        return ResponseModel.of(authorizationService.verifyToken(jwtToken));
    }

    @RequestMapping(value = "services/apexrest/invioRipristino", method = RequestMethod.POST)
    @ApiOperation(value = "Cerca una person per id", notes = "Ritorna un PersonResource")
    public ResponseWorkOrderFixing getPersonById(
            @RequestBody RequestWorkOrderFixing req) throws NoSuchAlgorithmException {

        ResponseWorkOrderFixing response = new ResponseWorkOrderFixing();
        response.setCode("OK");
        response.setDescription("OK Description");
        log.debug("Ok call on invioFixingRipristino");

        return response;
    }

}
