package prv.app.login.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import prv.app.login.entity.MioInterceptor;
import prv.app.login.resource.MioTrack;
import prv.app.login.resource.RequestWorkOrderFixing;
import prv.app.login.resource.ResponseModel;
import prv.app.login.resource.ResponseWorkOrderFixing;
import prv.app.login.service.AuthorizationService;
import prv.app.login.service.InterceptorService;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/")
@Api(value = "views")
public class InterceptorController {


    InterceptorService interceptorService;
    AuthorizationService authorizationService;

    public InterceptorController(AuthorizationService authorizationService,
                                 InterceptorService interceptorService){
        this.authorizationService = authorizationService;
        this.interceptorService = interceptorService;
    }

    @GetMapping(path = "getTracks")
    @ApiOperation(value = "retrieve all tracks")
    public ResponseModel<List<MioTrack>> checkAllTracks() {

        log.info("ZoneId: {} " , ZoneId.systemDefault());
        ResponseModel<List<MioTrack>> result =  ResponseModel.of(interceptorService.getAll());
        return result;
    }

    @PostMapping(path = "getFilteredTracks")
    @ApiOperation(value = "retrieve filtered tracks")
    public ResponseModel<List<MioTrack>> checkFilteredTracks(@RequestBody String myDate) throws ParseException {

        ResponseModel<List<MioTrack>> result =  ResponseModel.of(interceptorService.getAllMioInterceptorByCallDate(myDate));
        return result;
    }

}
