package prv.app.login.resource;


import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MioTrack implements Serializable {
    private String callDate;
    private String srvName;
    private String ip;
}
