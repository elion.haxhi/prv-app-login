package prv.app.login.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.DelegatingFilterProxy;
import prv.app.login.entity.User;
import prv.app.login.repository.UserRepository;
import prv.app.login.service.AuthorizationService;
import prv.app.login.service.InterceptorService;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

@Slf4j
@Component
public class FrontControllerFilter extends DelegatingFilterProxy {


    public static final String COOKIE_NAME = "prv_sso_cookie";

    @Inject
    PasswordEncoder passwordEncoder;
    @Inject
    AuthorizationService authorizationService;
    @Inject
    InterceptorService interceptorService;


    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        HttpSession httpSession = reNewTheSession((HttpServletRequest) request, (HttpServletResponse) response);
        httpSession.setAttribute("authenticated", Boolean.FALSE);

        String username = httpServletRequest.getParameter("username");
        String password = httpServletRequest.getParameter("password");
        String srvName = httpServletRequest.getParameter("srvName");


        Optional<User> user = authorizationService.findUserByUsername(username);

        addAuthenticatedAndSrvNameOnSession(httpSession, password, srvName, user.orElse(null));

        if ((Boolean) httpSession.getAttribute("authenticated")) {
            redirectToSrvName(httpServletRequest, httpServletResponse, username, password, httpSession);
        }
        chain.doFilter(request, response);
    }

    private HttpSession reNewTheSession(HttpServletRequest request,HttpServletResponse response) {

        HttpSession session =  request.getSession(true);
        if(request.getContextPath().equals("/prv-app-login") && session.getAttribute("srvName") == null){
            request.getSession(true).invalidate();
            session = request.getSession(true);
        }
        return session;
    }



    private void addAuthenticatedAndSrvNameOnSession(HttpSession httpSession, String password, String srvName, User user) {
        if (user != null && passwordEncoder.matches(password, user.getPassword()))
            httpSession.setAttribute("authenticated", Boolean.TRUE);

        if (srvName != null)
            httpSession.setAttribute("srvName", srvName);
    }

    private void redirectToSrvName(HttpServletRequest request, HttpServletResponse httpServletResponse,
                                   String username,
                                   String password,
                                   HttpSession httpSession) throws IOException {
        String token = authorizationService.generateToken(username, password);
        if (httpSession.getAttribute("srvName") != null) {
            interceptorService.invokeInterceptor(new Date(), httpSession.getAttribute("srvName").toString(), request.getRemoteAddr());
            httpServletResponse.sendRedirect(((String) httpSession.getAttribute("srvName")).concat("?token=").concat(token));
            httpSession.invalidate();
            log.info(" Added token to the session");
        }

    }
}
