package prv.app.login.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.net.URL;
import java.util.concurrent.TimeUnit;

@Configuration
@Slf4j
public class MvcConfig extends WebMvcConfigurationSupport {

    @Value("${root.classpath}")
    String rootClasspath;

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        registry.viewResolver(resolver);
    }
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations(getPathOfStaticFolder());

        registry.addResourceHandler("/**/login")
                .addResourceLocations(getPathOfStaticFolder());

        registry.addResourceHandler("/**/errorlogin")
                .addResourceLocations(getPathOfStaticFolder());

        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");

        registry.addResourceHandler("/css-error/**")
                .addResourceLocations("classpath:static/css-error/")
                .setCacheControl(CacheControl.maxAge(1, TimeUnit.HOURS).cachePublic());

    }

    protected String getPathOfStaticFolder(){
        URL url = null;
        try {

            ClassLoader cLoader = MvcConfig.class.getClassLoader();

            url = cLoader.getResource("static");

        } catch (Exception e){
            e.printStackTrace();
        }

        // Get the relative path of static folder
        String s = url.getPath();
        int diff = s.indexOf("prv-app-login");
        String root =s.substring(0,diff);
        String path ="file:" + root + rootClasspath;

        log.info("PATH:" + path);
        return path;
    }
}
