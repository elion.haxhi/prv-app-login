package prv.app.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import prv.app.login.entity.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Long>{


    @Query("select u from User u join fetch u.authorities where LOWER(u.username) = LOWER(:username) ")
    User readByUsername(@Param("username") String username);

    @Query("select u from User u join fetch u.authorities")
    List<User> findAll();
}
