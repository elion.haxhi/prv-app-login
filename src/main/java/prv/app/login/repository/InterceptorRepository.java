package prv.app.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import prv.app.login.entity.MioInterceptor;
import prv.app.login.entity.User;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface InterceptorRepository extends JpaRepository<MioInterceptor,Long> {

    @Query("select m from MioInterceptor m where m.callDate =:localDate")
    List<MioInterceptor> trovaPerCallDate(@Param(value = "localDate") Date date);
}
