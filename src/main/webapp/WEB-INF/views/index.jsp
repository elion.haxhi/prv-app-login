<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="template/includes.jsp" %>
<!doctype html>
<head>
    <title>prv-app-login</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" id="color-scheme" href="css/colors/salmon.css">


    <script src="js/modernizr.js"></script>
    <script src="js/jquery.min.js"></script>
</head>
<body>
<div id="wrapper">
    <header id="header" role="banner">
        <nav class="navbar navbar-white animated-dropdown ttb-dropdown" role="navigation">
            <div class="navbar-top clearfix">
                <div class="container">
                    <div class="pull-left">
                        <ul class="navbar-top-nav clearfix hidden-sm hidden-xs">
                            <li><a href="https://pranveraapp-com.gitbook.io/prv-app-login" target="_blank">Architecture</a></li>
                            <li><a href="https://gitlab.com/elion.haxhi/prv-app-login">Source Code</a></li>
                        </ul>
                        <div class="dropdown account-dropdown visible-sm visible-xs">
                            <a  href="https://pranveraapp-com.gitbook.io/prv-app-login" target="_blank" id="account-dropdown"  aria-expanded="true">
                                Architecture
                            </a>
                        </div>
                    </div>
                    <div class="pull-left">

                        <div class="dropdown account-dropdown visible-sm visible-xs">
                            <div class="dropdown currency-dropdown pull-right">
                                <a style="padding: 10px;"  href="https://gitlab.com/elion.haxhi/prv-app-login" id="currency-dropdown" aria-expanded="true">
                                    Source Code
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>

        <div class="container" style="padding: 50px"/>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">

                    <c:if test="${not empty error}">
                        <div class="alert alert-dismissable alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <span>${error}</span>
                        </div>
                    </c:if>

                    <div class="form-wrapper">
                        <h2 class="title-underblock custom mb30">Login</h2>
                        <form action="/prv-app-login/login" method="post">

                            <div class="form-group">
                                <label for="username" class="input-desc">Username</label>
                                <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
                            </div>
                            <div class="form-group mb10">
                                <label for="password" class="input-desc">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                            </div>
                            <div class="form-group mb5">
                                <input type="submit" class="btn btn-custom" value="Login Now">
                            </div>
                        </form>
                    </div>

                </div>

                <div class="mb40 visible-xs"></div>

                <div class="col-sm-6">
                    <h2 class="title-underblock custom mb40">SSO Authentication</h2>

                    <p>E' un applicativo responsabile per authenticare altri applicativi mediante la tecnica single sign on. In questo progetto viene usato java 8 ,lombok , maven e come framework JPA e SpringBootFramework. Il progetto è stato sviluppato con la tecnica TDD.</p>

                    <p>Questo applicativo contiene due funzionalita principali:</p>
                    <p> 1. Genera un token generato con la tencnologia jwt.</p>
                    <p> 2. Esponse un servizio rest per la validazione di un token.</p>

                    <div class="mb10">
                        <p><b>Username: prvapp</b> <br/> <b>Password: password1!</b></p>
                    </div>

                </div>
            </div>
        </div>

        <div class="mb40 mb20-xs"></div>

    </div>
</div>

<!-- END -->

<!-- Smoothscroll -->
<script src="js/smoothscroll.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.hoverIntent.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/waypoints-sticky.min.js"></script>
<script src="js/jquery.debouncedresize.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/twitter/jquery.tweet.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>
