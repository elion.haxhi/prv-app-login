
-- ALTER TABLE prv_user_authority DROP FOREIGN KEY fk_pua_u;
-- ALTER TABLE prv_user_authority DROP FOREIGN KEY fk_pua_a;

DROP TABLE IF EXISTS zz;
DROP TABLE IF EXISTS prv_user;
DROP TABLE IF EXISTS prv_authority;
DROP TABLE IF EXISTS prv_user_authority;
DROP TABLE IF EXISTS prv_interceptor;
