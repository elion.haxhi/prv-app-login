CREATE TABLE zz (
     id BIGINT(20) NOT NULL AUTO_INCREMENT,
     value INTEGER,
     primary key (id)
);

CREATE TABLE prv_user(
     id  BIGINT(20) NOT NULL AUTO_INCREMENT,
     username VARCHAR(25) NOT NULL,
     password VARCHAR(255) NOT NULL,
     PRIMARY KEY (id)
);

CREATE TABLE prv_authority(
     id  BIGINT(20) NOT NULL AUTO_INCREMENT,
     authority VARCHAR(25) NOT NULL,
     PRIMARY KEY (id)
);

CREATE TABLE prv_user_authority(
    prv_user_id BIGINT NOT NULL,
    prv_authority_id BIGINT NOT NULL
);

ALTER TABLE prv_user_authority
    ADD CONSTRAINT fk_pua_u FOREIGN KEY (prv_user_id)
        REFERENCES prv_user(id);

ALTER TABLE prv_user_authority
    ADD CONSTRAINT fk_pua_a FOREIGN KEY (prv_authority_id)
        REFERENCES prv_authority(id);

CREATE TABLE prv_interceptor(
     id  BIGINT(20) NOT NULL AUTO_INCREMENT,
     callDate date NOT NULL,
     srvName VARCHAR(255) NOT NULL,
     ip VARCHAR(255) NOT NULL,
     PRIMARY KEY (id)
);