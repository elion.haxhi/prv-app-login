INSERT INTO zz(id, value)
VALUES (1, 10000);

INSERT INTO prv_user(id, username, password)
VALUES(1, '3li0n', '$2a$10$UukYsg3cJzhGQERLdi4mm.0y0S7P7/7/vtLamlZf9ZYWxGz0gtRJu');
INSERT INTO prv_user(id, username, password)
VALUES(2, 'd0r1an', '$2a$10$UukYsg3cJzhGQERLdi4mm.0y0S7P7/7/vtLamlZf9ZYWxGz0gtRJu');
INSERT INTO prv_user(id, username, password)
VALUES(3, 'anonymous', '$2a$10$UukYsg3cJzhGQERLdi4mm.0y0S7P7/7/vtLamlZf9ZYWxGz0gtRJu');


INSERT INTO prv_authority(id, authority)
VALUES(10, 'ROLE_ADMIN');
INSERT INTO prv_authority(id, authority)
VALUES(11, 'ROLE_USER');

INSERT INTO prv_user_authority(prv_user_id, prv_authority_id)
VALUES(1, 10);
INSERT INTO prv_user_authority(prv_user_id, prv_authority_id)
VALUES(2, 10);
INSERT INTO prv_user_authority(prv_user_id, prv_authority_id)
VALUES(3, 11);

INSERT INTO prv_interceptor (id, callDate, srvName, ip) VALUES (1, '2021-07-06', 'a', '127.0.0.1');
INSERT INTO prv_interceptor (id, callDate, srvName, ip) VALUES (2, '2021-07-06', 'b', '127.0.0.2');


