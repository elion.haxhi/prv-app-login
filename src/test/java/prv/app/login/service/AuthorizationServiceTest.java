package prv.app.login.service;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import prv.app.login.UnitTest;
import prv.app.login.entity.Authority;
import prv.app.login.entity.User;
import prv.app.login.repository.UserRepository;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@Slf4j
@Category(UnitTest.class)
@RunWith(MockitoJUnitRunner.class)
public class AuthorizationServiceTest {

    @Mock
    UserRepository userRepository;
    @Mock
    User user;
    @Mock
    Authority authority;


    @InjectMocks
    AuthorizationService authorizationService = new AuthorizationService();




    @Test
    public void _1_2_1_GivenAUsernameWhenFindByUsernameIsCalledthenAUserShouldReturn(){
        when(user.getAuthorities()).thenReturn(Arrays.asList(authority));
        when(user.getUsername()).thenReturn("3li0n");
        when(user.getPassword()).thenReturn("secret");
        when(userRepository.readByUsername("3li0n")).thenReturn(user);

        Optional<User> optionalUser = authorizationService.findUserByUsername("3li0n");
        User user = optionalUser.get();

        assertNotNull(user);
        assertFalse(user.getAuthorities().isEmpty());
        assertFalse(user.getUsername().isEmpty());
        assertFalse(user.getPassword().isEmpty());
    }

    @Test
    public void _1_2_2_GivenAWrongUsernameWhenFindUserByUsernameIsCalledThenAnOptionalEmptyUserWillBeBack(){
        when(userRepository.readByUsername("wrong")).thenReturn(null);

        Optional<User> optionalUser = authorizationService.findUserByUsername("wrong");

        assertFalse(optionalUser.isPresent());
    }

    @Test
    public void _1_3_1_GivenAUsernameWhenGenerateTokenIsCalledThenAJwtTokenShouldBeReturned(){

        String jwtToken = authorizationService.generateToken("prvapp","password1!");
        assertNotNull(jwtToken);
        log.info(jwtToken);
    }

    @Test
    public void shouldReturnAStringGivenAMap(){
        Map<String,Object> map = new HashMap<>();
        map.put("a","b");
        String stringifyMap = authorizationService.encode(map);

        assertNotNull(stringifyMap);
    }

    @Test
    public void  _1_5_1_GivenAWrongJwtTokenWhenVerifyTokenIsCalledThenAFalseValueShouldBeReturned(){
        String wrongToken = "e3R5cD1KV1QsIGFsZz1IUzI1Nn1.e2lhdD0xNjE4MjI3NTI1LCB1c2VybmFtZT0zbGkwbn0.IFQ3WHfAc2v9I1sM4HPK4YQT1iWlqWaxTh34n40xqzE";

        log.info("JWTToken: " + wrongToken);
        boolean value = authorizationService.verifyToken(wrongToken);
        assertFalse(value);
    }
    @Test
    public void  _1_5_2_GivenAValidJwtTokenWhenVerifyTokenIsCalledThenATrueValueShouldBeReturned(){
        String validToken = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0.cHJ2YXBwLnBhc3N3b3JkMSEg.H4EjkcdR2RTHHTohZ6L05iw12UjmV8FWIGO3fMoS6BY";
        log.info("JWTToken: " + validToken);
        boolean value = authorizationService.verifyToken(validToken);
        assertTrue(value);
    }


    @Test
    public  void shouldEncodeAPassword(){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String value = bCryptPasswordEncoder.encode("admin");
        log.info(value);

    }

    @Test
    public void shouldDecodeAToken() throws JSONException {
//        String token = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0.e2lhdD0xNjE5MTEwNTY3LCB1c2VybmFtZT1lbGlvbn0.4ss3etcp2gEMzMfVSa3yfsdOwCHEVqcvodJTyRh87PU";
        String token = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0.cHJ2YXBwLnBhc3N3b3JkMSEg.H4EjkcdR2RTHHTohZ6L05iw12UjmV8FWIGO3fMoS6BY";
        String[] parts = token.split("\\.");
        String payload = decode(parts[1]);
        assertEquals("prvapp.password1! ", payload);

    }

    private static String decode(String encodedString) {
        return new String(Base64.getUrlDecoder().decode(encodedString));
    }

}