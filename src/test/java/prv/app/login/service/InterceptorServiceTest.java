package prv.app.login.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import prv.app.login.entity.MioInterceptor;
import prv.app.login.repository.InterceptorRepository;
import prv.app.login.resource.MioTrack;
import prv.app.login.resource.ResponseModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InterceptorServiceTest {

    @Mock
    InterceptorRepository interceptorRepository;
    @InjectMocks
    InterceptorService interceptorService;

    @Test
    public void itShouldInvokeTheInterceprorService(){
        Date date = new Date();


        String srvName = "http;//localhost:8080/prv-inv-api";
        String ip = "127.0.0.1";
        MioInterceptor myInterceptor = new MioInterceptor();
        myInterceptor.setId(1L);
        myInterceptor.setCallDate(date);
        myInterceptor.setSrvName(srvName);
        myInterceptor.setIp(ip);
        when(interceptorRepository.save((MioInterceptor) anyObject())).thenReturn(myInterceptor);

        MioInterceptor result = interceptorService.invokeInterceptor(date, srvName,ip);

        assertNotNull(myInterceptor);
        assertEquals(1L,result.getId());
        assertEquals(date,result.getCallDate());
    }

    @Test
    public void shouldGetAListOfMioInterceptorByLocalDate() throws ParseException {
        List<MioInterceptor> filteredMioInterceptor = Arrays.asList(
                buildMioInterceptor(2L,"29/07/2021","http://localhost:8080/prv-app-admin","127.0.0.2"),
                buildMioInterceptor(2L,"29/07/2021","http://localhost:8080/prv-app-admin","127.0.0.2")
        );
        when(interceptorRepository.trovaPerCallDate(anyObject())).thenReturn(filteredMioInterceptor);
        List<MioTrack> result = interceptorService.getAllMioInterceptorByCallDate("29/07/2021");
        assertNotNull(result);
        assertTrue(result.size()==2);
    }

    private Date generateDate(String myDate){
        Date parsedDate = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            parsedDate = sdf.parse(myDate);
        } catch (ParseException pe){
            pe.printStackTrace();
        }
        return parsedDate;
    }

    @Test
    public void shouldParseAListOfMioInterceptorToResource(){

        List<MioInterceptor> filteredMioInterceptor = Arrays.asList(
                buildMioInterceptor(2L,"28/07/2021","http://localhost:8080/prv-app-admin","127.0.0.2"),
                buildMioInterceptor(2L,"29/07/2021","http://localhost:8080/prv-app-admin","127.0.0.2")
        );
        ResponseModel<List<MioInterceptor>> result = interceptorService.parseToDto(filteredMioInterceptor);
        assertNotNull(result);
    }

    private MioInterceptor buildMioInterceptor(long id, String date, String srvName, String ip){
        return MioInterceptor.builder()
                             .id(id)
                             .callDate(generateDate(date))
                             .srvName(srvName)
                             .ip(ip)
                             .build();
    }
}