package prv.app.login.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import prv.app.login.E2ETests;
import prv.app.login.resource.MioTrack;
import prv.app.login.resource.ResponseModel;

import java.text.ParseException;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;


@Slf4j
@Category(E2ETests.class)
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class InterceptorServiceIntegrationTest {

    @Autowired
    InterceptorService interceptorService;


    @Test
    public void shoulReturnAListOfMioInterceptors() throws ParseException {
        List<MioTrack> result = interceptorService.getAllMioInterceptorByCallDate("06/07/2021");
        ResponseModel<List<MioTrack>> responseModel = ResponseModel.of(result);
        log.info(responseModel.getEntity().toString());

    }

    @Test
    public void shouldReurnAllInterceptors() throws ParseException {
        List<MioTrack> result = interceptorService.getAll();
        assertFalse(result.isEmpty());
    }



}