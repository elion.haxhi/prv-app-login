package prv.app.login.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import prv.app.login.Integration;
import prv.app.login.entity.Authority;
import prv.app.login.entity.User;

import javax.inject.Inject;
import java.util.Optional;

import static org.junit.Assert.*;

@Slf4j
@SpringBootTest
@Category(Integration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorizationServiceIntegrationTest {


    @Inject
    AuthorizationService authorizationService;

    @Test
    public void _1_2_1_GivenAUsernameWhenFindByUsernameIsCalledthenAUserShouldReturn(){
        Optional<User> optionalUser = authorizationService.findUserByUsername("3li0n");
        User user = optionalUser.get();

        assertEquals("3li0n", user.getUsername());
        assertFalse(user.getAuthorities().isEmpty());
        assertFalse(user.getPassword().isEmpty());
        assertTrue(user.getAuthorities().contains(new Authority(10L, "ROLE_ADMIN")));
    }


    @Test
    public void _1_2_2_GivenAWrongUsernameWhenFindUserByUsernameIsCalledThenAnOptionalEmptyUserWillBeBack(){
        Optional<User> optionalUser = authorizationService.findUserByUsername("wrong");
        assertFalse(optionalUser.isPresent());
    }

}