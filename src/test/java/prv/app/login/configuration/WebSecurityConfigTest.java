package prv.app.login.configuration;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class WebSecurityConfigTest {

    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Test
    public void testPassword(){
        List<String> encryptedPasswords = Arrays.asList(
                "$2a$10$CiZNcncdRqZRedA401TVue5wNR5/ia8Yumg7cGbt3dfsjf1Qdb5Cm",
                "$2a$10$6R0nBZVSEptIguNo6KKtb.RPwj8MkDZ9cB5C//RtCYHvwQOlCatUi"
        );
        log.info(passwordEncoder.encode("password1!"));
        log.info(String.valueOf(passwordEncoder.matches("password1!",String.valueOf(encryptedPasswords.get(0)))));
        log.info(String.valueOf(passwordEncoder.matches("password1!",String.valueOf(encryptedPasswords.get(1)))));


    }


}