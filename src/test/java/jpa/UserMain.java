package jpa;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import prv.app.login.entity.Authority;
import prv.app.login.entity.MioInterceptor;
import prv.app.login.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

public class UserMain {

    static EntityManager em =null;
    static {
        System.setProperty("org.jboss.logging.provider", "jdk");// for more details comment this line
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_db_PU");
        em = emf.createEntityManager();
    }

    /**
     * Description utils: The META-INF folder is needed only for the purpose of working
     * with the domain model. As you see this class doesn't use tomcat and either junit
     * so it is a little project embedded inside the application.
     * When you run the test unit this class doesn't run and this is good. Here we talk with the real db
     * so be careful with the data in case when you change the state .Why i decided to do so? Because
     * it is much faster than the intergation test of junit. I talk with the database and i can create
     * the business logic.
     *
     * @param args
     */
    public static void main(String[] args) {
//        getMioInterceptor();
//        getAllMioInterceptors();
//        saveAUser(args);
//        readAllUser(args);
//        readAllAuthorities(args);
//        getAuthorityById(args);
//        readUserByName(args);
        System.exit(0);
    }

    public static void readUserByName(String[] args) {
        User user = (User)em.createNamedQuery("el_read_user_by_name")
                .setParameter("username","elion")
                .getSingleResult();
        System.out.println(user);
        List<Authority> authorities = user.getAuthorities();
    }



    public static void getAuthorityById(String[] args) {
        Authority authority = em.find(Authority.class, 1L);
        System.out.println(authority);
    }

    public static void readAllAuthorities(String[] args) {
        List<User> list = em.createNamedQuery("el_read_all_authorities").getResultList();
        System.out.println(list);
    }

    public static void readAllUser(String[] args) {
        List<User> list = em.createNamedQuery("el_read_all_user").getResultList();
        System.out.println(list);
    }

    public static void saveAUser(String[] args) {

        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encryptedPAssword = passwordEncoder.encode("admin");

        Authority  a1 = em.find(Authority.class, 1L);
        List<Authority> authorities = Arrays.asList(a1);

        User user = User.builder().password(encryptedPAssword).username("mario").authorities(authorities).build();
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
    }

    private static void getAllMioInterceptors() {
        List<MioInterceptor> list = em.createNamedQuery("EL_READ_ALL_MIOINTERCEPTOR").getResultList();
        System.out.println(list);
    }

    private static void getMioInterceptor() {
        MioInterceptor mioInterceptor = em.find(MioInterceptor.class,1l);
        System.out.println(mioInterceptor);
    }

}
