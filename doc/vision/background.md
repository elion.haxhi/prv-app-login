# Background

#### System Overview

C’e’ la necessità di creare un app con la responsabilità di autenticare vari utenti che
vogliono utilizzare gli applicativi del PranveraApp factory. Tutti gli applicativi
comunicano via protocollo https.

#### Goals and Context

L’applicativo prv-app-login deve essere responsabile per autenticare utenti che
accedono mediante username a password. L’app deve verificare che l’utente sia un
utente reale se si allora prv-app-login invia un token come parametro al l’applicativo
a cui vuole accedere. Quindi prv-app-login deve fare due cose:

* Staccare i token jwt
* Validare i rispettivi token jwt

La figura 1 ci illustra ad alto livello la struttura architetturale di prv-app-login e i suoi
partecipanti.

<figure><img src="../../img/figura1.jpg" alt=""><figcaption></figcaption></figure>

