# Use Case Login

Il login da la possibilità di accedere a servizi protetti di altre app.

* L’utente vuole accedere la app1 quindi l’applicazione(app1) come primo
passo tenta di ottenere il cookie corrispondente dalla request, se il cookie
non esiste allora la app1 invoca prv-app-login. 
* L’utente inserisce lo username e la password e fa la submit della request. 
* La prv-app-login fa le verifiche per autenticare l’utente, se le verifiche sono
   ok allora la prv-app-login genera un token lo setta nel cookie e fa il redirect
   verso la app1.