# Use Case – CheckToken

Doppo il login di successo l’applicativo avrà una session di durata 2 ore. Le app verranno
invocate sempre con un parametro jwt token. 
* Per le app che espongo solo servizi API, si farà un check del token
   autenticazione chiamando il servizio esposto da prv-app-login se la risposta è
   vera allora le API daranno servizio se invece la risposta è falsa un popup di
   authorization/authentication si evidenzierà.