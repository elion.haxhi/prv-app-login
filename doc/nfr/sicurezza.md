# Sicurezza

Le chiamate di autenticazione devono essere fate sotto un canale https. Non sarà
possibile vedere il plain text della password la verifica sarà fatta sulla password cifrata
mediante un algoritmo di cifratura.