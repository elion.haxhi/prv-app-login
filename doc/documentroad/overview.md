# Overview

1.1. Come è organizzato il documento
* Documentation Roadmap: fornisce informazioni riguardanti questo documento
* Document Vision: fornisce informazioni del architettura del applicativo , descrive i
requisiti del business e la composizione generica del impianto.
* Functional Requirements: Descrive i servizi chiesti dal business
* Non Functional Requirements: Descrive le varie problematiche richieste dal
business
* Use Cases: descrive I requisiti funzionali mediante gli use cases
* Activity diagram: descrive i comportamenti del sistema per soddisfare i requisiti
* Refinement: descrive gli aggiornamenti dei use case
* High level solution: descrive la soluzione mediante i diagrammi UML