# Description

Nella figura 3 vediamo il flusso principale che di login.

<figure><img src="../../img/figura3.jpg" alt=""><figcaption></figcaption></figure>

Nella figura 3.1 vediamo la procedura di checkToken. Questo endpoint dovrà essere
chiamato dalle app alle quali è stato richiesto servizio e perciò eseguiranno la procedura di
verifica token.

<figure><img src="../../img/figura3.1.jpg" alt=""><figcaption></figcaption></figure>
