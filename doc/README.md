---
icon: hand-wave
---

# Introduction

### Storico del Documento

| **Versione** | **Data**                    | **Autore**     | **Commenti**     |
|--------------|-----------------------------|----------------|------------------|
| 1.0.0        | 05/03/2021 | 1. Elion Haxhi | Primo Rilascio   |
| 1.0.1        | 05/03/2021 | 2. Elion Haxhi | Correzione testo |

Questa wiki serve per illustrare l’architettura del’applicativo prv-app-login.\
E’ una app accessibile con i browser comuni come chrome firefox, internet explorer.\

1.1. Come è organizzato il documento\
* Documentation Roadmap: fornisce informazioni riguardanti questo documento
* Document Vision: fornisce informazioni del architettura del applicativo , descrive i
requisiti del business e la composizione generica del impianto.
* Functional Requirements: Descrive i servizi chiesti dal business
* Non Functional Requirements: Descrive le varie problematiche richieste dal
business
* Use Cases: descrive I requisiti funzionali mediante gli use cases
* Activity diagram: descrive i comportamenti del sistema per soddisfare i requisiti
* Refinement: descrive gli aggiornamenti dei use case
* High level solution: descrive la soluzione mediante i diagrammi UML

1.2 Skateholder coinvolti:

| **Numero** | **Mansione** |
|------------|--------------|
| 1.         | Project Manager   |
| 2.         | Business Experts|
| 3.         | Software Architects|
| 4.         | Software Developers|
