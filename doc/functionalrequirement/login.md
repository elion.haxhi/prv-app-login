# Login

Quando un utente vuole autenticarsi su una app che fa parte del insieme PranveraApp
allora l’applicativo prv-app-login deve entrare in scena è l’utente si troverà davanti una
form dove potrà inserire le sue credenziali. Se l’autenticazione va a buon fine allora
mediante un meccanismo di redirect l’utente potrà usare la sua app di interesse. 