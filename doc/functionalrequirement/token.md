# Check Token

Se un utente autenticato precedentemente chiude il browser e in un secondo momento lo
riapre e cerca di usare un app del insieme dei applicativi PranveraApp allora l’accesso
sarà automatico non ci sarà bisogno di autenticarsi di nuovo. Se esiste il cookie allora
entrerà in gioco la procedura di autenticazione in modo “silent background”. Ovvero l’app
ricaverà le credenziali di accesso dal token e successivamente gli aggiungerà nel
SpringContext per le app con framework spring invecce per quelle di tipo JavaEE vera
usato una chiamata al database per ottenere il profilo utente.
