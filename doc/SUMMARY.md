# Table of contents

* [Introduction](README.md)

## DocumentRoad

* [Overview](documentroad/overview.md)

## Vision
* [Background](vision/background.md)

## Functional Requirements
* [Login](functionalrequirement/login.md)
* [CheckToken](functionalrequirement/token.md)

## Non Functional Requirements
* [Overview](usecases/overview.md)
* [UC Login](usecases/uclogin.md)
* [UC CheckToken](usecases/ucchecktoken.md)

## Activity Diagram
* [Description](activity/description.md)

## High Level Solution
* [Assumtion](highlevel/assumtion.md)
* [Risk And Mitigation](highlevel/mitigation.md)
* [Sequence Diagram](highlevel/sequence.md)
  * [Sequence Login](highlevel/sequence/sequencelogin.md)
  * [Sequence Check Token](highlevel/sequence/sequencechecktoken.md)

## Class Diagram
* [Class Diagram](classdiagram/description.md)

## Component Diagram
* [Component Diagram](componentdiagram/description.md)

## Deployment Diagram
* [Deployment Diagram](deploymenddiagram/descritpion.md)

## MISC
* [Website](https://pranveraapp.com)
* [Gitlab](https://gitlab.com/elion.haxhi)
* [Blog](https://pranveraapp.com/papp/news/)





