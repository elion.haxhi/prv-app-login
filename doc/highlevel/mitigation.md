# Mitigation

| **Risk** | **Mitigation**                                                                          |
|--------|-----------------------------------------------------------------------------------------|
| I dati sensibili del utente viaggiano in chiaro.| Tutte le chiamate verso la prv-app-login devono essere fatte sotto il protocollo https. |
